#!/usr/bin/env bash

set -e

if [ -z "${BACKUP_FILE}" ]; then
  echo "==# I need an backups file to work with"
  exit 1
fi

tar xjf "${BACKUP_FILE}"

backups_dir="$( echo "${BACKUP_FILE}" | sed -e "s/\..*$//" )"
cd "${backups_dir}"

while read dumpFile; do
  db="$( echo ${dumpFile} | sed -e "s/\.sql//g" )"

  echo "==> Importing db ${db} from ${dumpFile}"

  sed -i \
    -e "s|/\*\!50017 DEFINER=.*\*/ ||" \
    -e "s/DEFINER=\`.*\`@\`\(%\|localhost\|127\.0\.0\.1\)\` / /g" \
    "${dumpFile}"

  echo "create database \`${db}\`" | mysql --force

  mysql $db < "${dumpFile}"
done < <(ls)

while read dumpFile; do
  db="$( echo ${dumpFile} | sed -e "s/\.sql//g" )"

  echo "==> Importing db ${db} from ${dumpFile} second time"

  mysql $db < "${dumpFile}"
done < <(ls | grep "vinderijnl\|poetsstudentnl\|oppasstudentnl\|seniorenstudentnl" )

mysql -e "FLUSH PRIVILEGES;"
