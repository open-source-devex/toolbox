#!/usr/bin/env bash

get-instance-id() {
  cat ".restored-db-instance-id"
}


set -e

DB_INSTANCE=$1

if [ -z "${DB_INSTANCE}" ]; then
  DB_INSTANCE="$( get-instance-id )"
  if [ -z "${DB_INSTANCE}" ]; then
    echo "==# I need a restored DB instance id to work with"
    exit 1
  fi
fi

if [[ "${DB_INSTANCE}" != restored-* ]]; then
  echo "==# DB instance (${DB_INSTANCE}) name does not indicate that is a restore"
  exit 2
fi

echo "==> Deleting ${DB_INSTANCE}"
aws rds delete-db-instance --skip-final-snapshot --db-instance-identifier "${DB_INSTANCE}"

echo "==> Waiting for ${DB_INSTANCE} to be deleted"
aws rds wait db-instance-deleted --db-instance-identifier "${DB_INSTANCE}"

echo "==> DB instance ${DB_INSTANCE} deleted"
