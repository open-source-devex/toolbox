#!/usr/bin/env bash

set -e

ENVIRONMENT="$1"
DOMAIN_NAME="$2"
DATA_MOUNT_POINT="$3"

if [ -z "${ENVIRONMENT}" ]; then
  echo "==# I need an environment to work with"
  exit 1
fi

if [ -z "${DOMAIN_NAME}" ]; then
  echo "==# I need a domain name to work with"
  exit 1
fi

if [ -z "${DATA_MOUNT_POINT}" ]; then
  echo "==# I need an mount point to work with"
  exit 1
fi

echo "==> Mounting ${ENVIRONMENT} EFS on ${DATA_MOUNT_POINT}"
mkdir -p "${DATA_MOUNT_POINT}/${ENVIRONMENT}"
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 "efs.${ENVIRONMENT}.${DOMAIN_NAME}:/" "${DATA_MOUNT_POINT}/${ENVIRONMENT}"
echo "==> Mounted"
