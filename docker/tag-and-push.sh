#!/usr/bin/env bash

SRC_IMAGE=${1}
DST_IMAGE=${2}

if [[ -z "${SRC_IMAGE}" ]]; then
    echo "==# Need SRC_IMAGE as 1st arg"
    exit 1
fi

if [[ -z "${DST_IMAGE}" ]]; then
    echo "==# Need DST_IMAGE as 2nd arg"
    exit 1
fi

echo "==> Tagging ${SRC_IMAGE} as ${DST_IMAGE}"
docker pull "${SRC_IMAGE}"
docker tag "${SRC_IMAGE}" "${DST_IMAGE}"
docker push "${DST_IMAGE}"
