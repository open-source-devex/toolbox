#!/usr/bin/env bash

get-snapshot-id() {
  cat .snapshot-id
}

set -e

SNAPSHOT_ID=$1

if [ -z "${SNAPSHOT_ID}" ]; then
  SNAPSHOT_ID="$( get-snapshot-id )"
  if [ -z "${SNAPSHOT_ID}" ]; then
    echo "==# I need a snapshot id to work with"
    exit 1
  fi
fi

echo "==> Deleting snapshot ${SNAPSHOT_ID}"
aws rds delete-db-snapshot --db-snapshot-identifier "${SNAPSHOT_ID}"

echo "==> Snapshot ${SNAPSHOT_ID} will be deleted"
