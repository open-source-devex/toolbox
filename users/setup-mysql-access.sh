#!/usr/bin/env bash

set -e

script_dir="$( cd "$( dirname "${0}" )" && pwd )"

DB_HOST=$1
DB_USER=$2

if [[ -z "${DB_HOST}" ]]; then
  echo "==# Need a mysql host to configure"
  exit 1
fi

if [[ -z "${DB_USER}" ]]; then
  echo "==# Need a mysql user to configure"
  exit 1
fi

if [[ -z "${DB_PASS}" ]]; then
  echo "==> No password supplied"
fi


config_file=~/.my.cnf

if [[ -f "${config_file}" && ! -f "${config_file}_backup" ]]; then
  cp "${config_file}" "${config_file}_backup"
fi

cp "${script_dir}/configuration/mysql_config" "${config_file}"
sed -i -e "s/host=/host=${DB_HOST}/" -e "s/user=/user=${DB_USER}/" "${config_file}"

if [[ ! -z  "${DB_USER}" ]]; then
  echo -e "password=\"${DB_PASS}\"" >> "${config_file}"
fi

echo "==> MySQL access to ${DB_HOST} configured"
