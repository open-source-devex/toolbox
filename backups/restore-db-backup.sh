#!/usr/bin/env bash

get-decrypted-file-name() {
  cat .decrypted-file
}

set -e

if [ -z "${BACKUP_FILE}" ]; then
  echo "==# I need an backup file to work with"
  exit 1
fi

if [ -z "${BUCKET}" ]; then
  echo "==# I need an S3 bucket to work with"
  exit 1
fi

if [ -z "${DB_HOST}" ]; then
  echo "==# I need an db host to work with"
  exit 1
fi

if [ -z "${DB_USER}" ]; then
  echo "==# I need an user name to access the DB instance"
  exit 1
fi

if [ -z "${DB_PASS}" ]; then
  echo "==# I expected a password to access the DB instance"
fi

if [ -z "${AWS_ACCESS_KEY_ID}" ]  ||
   [ -z "${AWS_DEFAULT_REGION}" ] ||
   [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
  echo "==# I need AWS credentials to work with"
  exit 1
fi

if [ -z "${ENCRYPTION_PASSWORD}" ]; then
  echo "I need a password to encrypt the DB backups"
  exit 1
fi

scripts_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."

"${scripts_dir}/aws/download-file-from-s3.sh" "${BUCKET}" "${BACKUP_FILE}"

"${scripts_dir}/users/setup-mysql-access.sh" "${DB_HOST}" "${DB_USER}"

"${scripts_dir}/crypto/decrypt-file.sh" "${ENCRYPTION_PASSWORD}" "${BACKUP_FILE}"
export BACKUP_FILE=$( get-decrypted-file-name )

"${scripts_dir}/db/restore-dbs.sh"
