#!/usr/bin/env bash

set -e

if [[ -z "${SETUP_AWS_PROFILE}" ]]; then

  exit 0;

fi

rm -rf ~/.aws
mkdir ~/.aws

cat <<EOF >> ~/.aws/credentials
[${SETUP_AWS_PROFILE}]
aws_access_key_id = ${SETUP_AWS_ACCESS_KEY_ID}
aws_secret_access_key = ${SETUP_AWS_SECRET_ACCESS_KEY}
EOF
