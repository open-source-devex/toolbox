#!/usr/bin/env bash

VERSION=${1}
VERSION_FILE=${2}

if [[ -z "${VERSION}" ]]; then
    echo "==# Need VERSION as 1st arg"
    exit 1
fi

if [[ -z "${VERSION_FILE}" ]]; then
    echo "==# Need VERSION_FILE as 2nd arg"
    exit 1
fi

if [[ -z "$( git branch | grep '^* master$' )" ]]; then
    echo "==> Checking out master branch"
    git branch -d master
    git fetch origin master:master
    git checkout master
fi

echo "==> Bump version to ${VERSION} and push new commit"
bump.sh ${VERSION} > ${VERSION_FILE}
git add ${VERSION_FILE}
git commit -m "Incremented version from ${VERSION} to $( cat ${VERSION_FILE} )"
git push origin master
