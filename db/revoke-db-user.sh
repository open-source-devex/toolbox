#!/usr/bin/env bash

set -e

if [ -z "${REVOKE_USER}" ]; then
  echo "==# I need an user to create (REVOKE_USER)"
  exit 1
fi

if [ -z "${REVOKE_USER_HOST}" ]; then
  echo "==# I expected a host for user, will use wildcard '%' (REVOKE_USER_HOST)"
  REVOKE_USER_HOST='%'
fi

if [ -z "${REVOKE_DB}" ]; then
  echo "==# I need a DB to revoke user privileges on (REVOKE_DB)"
  exit 1
fi

if [ -z "${REVOKE_PRIVILEGES}" ]; then
  echo "==# I privileges to revoke to the user (REVOKE_PRIVILEGES)"
  exit 1
fi

if [ -z "${DB_HOST}" ]; then
  echo "==# I need an db host to work with"
  exit 1
fi

if [ -z "${DB_USER}" ]; then
  echo "==# I need an user name to access the DB instance"
  exit 1
fi

if [ -z "${DB_PASS}" ]; then
  echo "==# I expected a password to access the DB instance"
fi

MYSQL_CREDENTIALS="-h ${DB_HOST} -u ${DB_USER}"
if [ ! -z "${DB_PASS}" ]; then
  MYSQL_CREDENTIALS="${MYSQL_CREDENTIALS} -p${DB_PASS}"
fi

echo "==> Revoking ${REVOKE_PRIVILEGES} to user '${REVOKE_USER}'@'${REVOKE_USER_HOST}' on ${REVOKE_DB}"
mysql ${MYSQL_CREDENTIALS} -e "REVOKE ${REVOKE_PRIVILEGES} ON ${REVOKE_DB}.* FROM '${REVOKE_USER}'@'${REVOKE_USER_HOST}';"
