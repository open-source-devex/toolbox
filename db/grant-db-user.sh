#!/usr/bin/env bash

set -e

if [ -z "${GRANT_USER}" ]; then
  echo "==# I need an user to create (GRANT_USER)"
  exit 1
fi

if [ -z "${GRANT_USER_HOST}" ]; then
  echo "==# I expected a host for user, will use wildcard '%' (GRANT_USER_HOST)"
  GRANT_USER_HOST='%'
fi

if [ -z "${GRANT_DB}" ]; then
  echo "==# I need a DB to grant user privileges on (GRANT_DB)"
  exit 1
fi

if [ -z "${GRANT_PRIVILEGES}" ]; then
  echo "==# I privileges to grant to the user (GRANT_PRIVILEGES)"
  exit 1
fi

if [ -z "${DB_HOST}" ]; then
  echo "==# I need an db host to work with"
  exit 1
fi

if [ -z "${DB_USER}" ]; then
  echo "==# I need an user name to access the DB instance"
  exit 1
fi

if [ -z "${DB_PASS}" ]; then
  echo "==# I expected a password to access the DB instance"
fi

MYSQL_CREDENTIALS="-h ${DB_HOST} -u ${DB_USER}"
if [ ! -z "${DB_PASS}" ]; then
  MYSQL_CREDENTIALS="${MYSQL_CREDENTIALS} -p${DB_PASS}"
fi

echo "==> Granting ${GRANT_PRIVILEGES} to user '${GRANT_USER}'@'${GRANT_USER_HOST}' on ${GRANT_DB}"
mysql ${MYSQL_CREDENTIALS} -e "GRANT ${GRANT_PRIVILEGES} ON ${GRANT_DB}.* TO '${GRANT_USER}'@'${GRANT_USER_HOST}';"
