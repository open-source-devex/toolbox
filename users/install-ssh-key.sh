#! /bin/bash

set -e

SSH_KEY=$1

if [[ -z "${SSH_KEY}" ]]; then
  echo "==# Need an SSH key to install"
  exit 1
fi

mkdir -p ~/.ssh
echo "${SSH_KEY}" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

echo "==> Key installed"
