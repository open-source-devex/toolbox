#!/usr/bin/env bash

set -e

if [ -z "${NEW_USER}" ]; then
  echo "==# I need an user to create (NEW_USER)"
  exit 1
fi

if [ -z "${NEW_USER_HOST}" ]; then
  echo "==# I expected a host for the new user, will use wildcard '%' (NEW_USER_HOST)"
  NEW_USER_HOST='%'
fi

if [ -z "${NEW_PASS}" ]; then
  echo "==# I expected a password for the new user to create (NEW_PASS)"
fi

if [ -z "${DB_HOST}" ]; then
  echo "==# I need an db host to work with"
  exit 1
fi

if [ -z "${DB_USER}" ]; then
  echo "==# I need an user name to access the DB instance"
  exit 1
fi

if [ -z "${DB_PASS}" ]; then
  echo "==# I expected a password to access the DB instance"
fi

MYSQL_CREDENTIALS="-h ${DB_HOST} -u ${DB_USER}"
if [ ! -z "${DB_PASS}" ]; then
  MYSQL_CREDENTIALS="${MYSQL_CREDENTIALS} -p${DB_PASS}"
fi

echo "==> Creating user ${NEW_USER}"
mysql ${MYSQL_CREDENTIALS} -e "CREATE USER '${NEW_USER}'@'${NEW_USER_HOST}' IDENTIFIED BY '${NEW_PASS}';"
