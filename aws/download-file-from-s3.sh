#!/usr/bin/env bash

set -e

BUCKET=$1
FILE=$2

if [ -z "${BUCKET}" ]; then
  echo "==# Need an S3 bucket to work with"
  exit 1
fi

if [ -z "${FILE}" ]; then
  echo "==# I need an file to work with"
  exit 1
fi

aws s3 cp s3://"${BUCKET}/${FILE}" "${FILE}"
