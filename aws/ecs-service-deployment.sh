#!/usr/bin/env bash

set -e
set -v

if [ -z "${CLUSTER}" ]; then
  echo "==# I need an ECS cluster to work with"
  exit 1
fi

if [ -z "${SERVICE}" ]; then
  echo "==# I need an ECS service to work with"
  exit 1
fi

if [ -z "${VERSION}" ]; then
  echo "==# I need an ECS service version to work with"
  exit 1
fi


timeout=7200

# find the service arn
service_arn="$( aws ecs list-services --cluster "${CLUSTER}" | jq -r '.serviceArns | map(select(endswith("'${SERVICE}'")))[0]' )"

# find the current task definition
task_definition_arn="$( aws ecs describe-services --cluster "${CLUSTER}" --services "${service_arn}" | jq -r '.services[0].taskDefinition' )"

# make the new task definition by replacing the version number in the current task definition
new_task_definition="$( aws ecs describe-task-definition --task-definition "${task_definition_arn}" | sed -e "s|\"image\": \"\(.*\):.*\"|\"image\": \"\1:${VERSION}\"|" | jq '.taskDefinition' )"

# JQ filter to remove all fields that are not needed from the new task definition
new_def_jq_filter="family: .family, volumes: .volumes, containerDefinitions: .containerDefinitions"

# apply JQ filter to new task definition
new_task_definition="$( echo "${new_task_definition}" | jq "{${new_def_jq_filter}}" )"

# register the new task definition
new_task_definition_arn="$( aws ecs register-task-definition --cli-input-json  "${new_task_definition}" | jq -r '.taskDefinition.taskDefinitionArn' )"

# update service with new task definition
aws ecs update-service --cluster "${CLUSTER}" --service "${service_arn}" --task-definition "${new_task_definition_arn}"

# wait until new version has replaced current version
timeout -t ${timeout} aws ecs wait services-stable --cluster "${CLUSTER}" --services "${service_arn}"

# notify after deployment
if [ $? -eq 0 ]; then
    message="Successfully deployed ${SERVICE} version ${VERSION} to ${CLUSTER}."
else
    message="Failed to deploy ${SERVICE} version ${VERSION} to ${CLUSTER} failed"
fi
echo "==> ${message}"
