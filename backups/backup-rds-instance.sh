#!/usr/bin/env bash

get-db-endpoint() {
  cat .db-endpoint
}

get-db-dump-file-name() {
  cat .db-dump-file-name
}

set -e

if [ -z "${ENVIRONMENT}" ]; then
  echo "==# I need an environment to work with"
  exit 1
fi

if [ -z "${BUCKET}" ]; then
  echo "==# I need an S3 bucket to work with"
  exit 1
fi

if [ -z "${DB_USER}" ]; then
  echo "==# I need an user name to access the DB instance"
  exit 1
fi

if [ -z "${DB_PASS}" ]; then
  echo "==# I expected a password to access the DB instance"
fi

if [ -z "${AWS_ACCESS_KEY_ID}" ]  ||
   [ -z "${AWS_DEFAULT_REGION}" ] ||
   [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
  echo "==# I need AWS credentials to work with"
  exit 1
fi

if [ -z "${ENCRYPTION_PASSWORD}" ]; then
  echo "I need a password to encrypt the DB backups"
  exit 1
fi

scripts_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."

"${scripts_dir}/aws/make-db-snapshot.sh" "${ENVIRONMENT}"

"${scripts_dir}/aws/start-instance-from-snapshot.sh"

db_host="$( get-db-endpoint )"
"${scripts_dir}/users/setup-mysql-access.sh" "${db_host}" "${DB_USER}"

"${scripts_dir}/db/dump-dbs.sh" "${ENVIRONMENT}"

"${scripts_dir}/crypto/encrypt-file.sh" "${ENCRYPTION_PASSWORD}" "$( get-db-dump-file-name )"

cp .encrypted-file .s3-file-name
"${scripts_dir}/aws/upload-file-to-s3.sh" "${BUCKET}"

"${scripts_dir}/aws/delete-restored-db-instance.sh"
"${scripts_dir}/aws/delete-db-snapshot.sh"
