#!/usr/bin/env bash

set -e

SCRIPT_NAME=$( basename "$0" )

usage() {

    echo "==# Usage: ${SCRIPT_NAME} hosted-zone-id record-set-name record-set-identifier"
}

HOSTED_ZONE_ID="$1"
RECORD_SET_NAME="$2"
SET_IDENTIFIER="$3"

if [ -z "${HOSTED_ZONE_ID}" ]; then

    echo "==# Need a hosted zone id as first argument"
    usage
    exit 1

fi

if [ -z "${RECORD_SET_NAME}" ]; then

    echo "==# Need a record set name as second argument"
    usage
    exit 1

fi

if [ -z "${SET_IDENTIFIER}" ]; then

    echo "==# Need a set identifier for the record name as third argument"
    usage
    exit 1

fi

echo "==> Looking for record"
RECORD="$( aws route53 list-resource-record-sets --hosted-zone-id "${HOSTED_ZONE_ID}" \
    --query "ResourceRecordSets[?Name == '${RECORD_SET_NAME}.']" \
    --query "ResourceRecordSets[?SetIdentifier == '${SET_IDENTIFIER}']" | jq -r '.[0]' )"

if [ -z "${RECORD}" ]; then

    echo "==# Could not find DNS record to delete"
    exit 1

fi

echo "==> Deleting record"
CHANGE_INFO_ID="$( aws route53 change-resource-record-sets --hosted-zone-id "${HOSTED_ZONE_ID}" --change-batch '
{
  "Changes": [
    {
      "Action": "DELETE",
      "ResourceRecordSet": '"${RECORD}"'
    }
  ]
}
' | jq -r '.ChangeInfo .Id')"

echo "==> Waiting for change to complete"
aws route53 wait resource-record-sets-changed --id "${CHANGE_INFO_ID}"
