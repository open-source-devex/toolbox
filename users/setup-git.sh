#!/usr/bin/env bash

set -e

if [[ -z "${BOT_NAME}" ]]; then
  BOT_NAME="bot"
fi

if [[ -z "${BOT_EMAIL}" ]]; then
  BOT_EMAIL="bot@notmail.null"
fi

if type git; then
  # git command is available
  git config --global user.name "${BOT_NAME}"
  git config --global user.email "${BOT_EMAIL}"
fi

if type git && git status; then
  if [[ -z "${GIT_REMOTE}" ]]; then
    echo "==# Need GIT_REMOTE to set git remote push url"
  else
    # git command is available and we are in a git repo
    git config --global push.default simple
    git config --global push.followTags true
    git remote set-url --push origin ${GIT_REMOTE}
  fi
fi
