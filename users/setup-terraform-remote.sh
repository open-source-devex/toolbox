#!/usr/bin/env bash

set -e

if [[ -z "${TERRAFORM_REMOTE_TOKEN}" ]]; then

  exit 0;

fi

cat <<EOF >> ~/.terraformrc
credentials "app.terraform.io" {
  token = "${TERRAFORM_REMOTE_TOKEN}"
}
EOF
