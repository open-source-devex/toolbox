#!/usr/bin/env bash

get-archive-file-name() {
  cat .archive-file-name
}

set -e

TARGET_FILE="$1"

if [ -z "${TARGET_FILE}" ]; then
  echo "==# I need an target file to work with"
  exit 1
fi

dirname="$( dirname "${TARGET_FILE}" )"
basename="$( basename "${TARGET_FILE}" )"
current_dir="$( pwd )"

if [ "${current_dir}" == "/" ]; then
  current_dir=""
fi

output_file="${basename}-$( date +%Y%m%d%H%M%S ).tar.bz"
output_file_path="${current_dir}/${output_file}"
echo "==> Archiving ${TARGET_FILE}"
cd "${dirname}"

# with "--warning=no-file-changed" we suppress the warnings of changed files during archiving
# but the exit code is still 1, therefore we need to handle that
set +e # do not break on exit code != 0
sudo tar --warning=no-file-changed -cjf "${output_file_path}" "${basename}"
exitcode=$?
if [ "$exitcode" != "1" ] && [ "$exitcode" != "0" ]; then
    exit $exitcode
fi
set -e # break on exit code != 0


cd -
echo "${output_file}" > .archive-file-name
echo "==> ${TARGET_FILE} archived to ${output_file_path}"
