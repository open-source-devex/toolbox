#! /bin/bash

set -e

rm -rf ~/.ssh

echo "==> SSH config removed"

rm -rf ~/.git-credentials ~/.gitconfig

echo "==> Git config removed"

if [[ -d ~/.aws ]]; then
  rm -rf ~/.aws

  echo "==> AWS config removed"
fi

if [[ -f ~/.terraformrc ]]; then
  rm -f ~/.terraformrc

  echo "==> Terraform Remote config removed"
fi
