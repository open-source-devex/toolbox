#!/usr/bin/env bash

set -e

ENVIRONMENT="$1"
DATA_MOUNT_POINT="$2"

if [ -z "${ENVIRONMENT}" ]; then
  echo "==# I need an environment to work with"
  exit 1
fi

if [ -z "${DATA_MOUNT_POINT}" ]; then
  echo "==# I need an mount point to work with"
  exit 1
fi

echo "==> Unmountin ${ENVIRONMENT} EFS on ${DATA_MOUNT_POINT}"
sudo umount "${DATA_MOUNT_POINT}/${ENVIRONMENT}"
echo "==> Unmounted"
