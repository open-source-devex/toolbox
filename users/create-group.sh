#!/usr/bin/env bash

set -e

GROUP_FILE="$1"

stdin_dump_file="/tmp/group-stdin-dump"

if [[ -z "${GROUP_FILE}" ]]; then
  echo "==> No input file given, will read from stdin"
  echo "$(</dev/stdin)" > ${stdin_dump_file}
  GROUP_FILE="${stdin_dump_file}"
fi

if ! cat ${GROUP_FILE} | jq -e . >/dev/null 2>&1; then
    echo "==# Failed to parse JSON content in ${GROUP_FILE}"
    exit 1
fi

name=$( jq -r '.name' ${GROUP_FILE} )
gid=$( jq -r '.gid' ${GROUP_FILE} )

# Create group
groupadd -g "${gid}" "${name}"

echo "==> Group ${name} created"
