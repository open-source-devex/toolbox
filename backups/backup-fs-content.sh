#!/usr/bin/env bash

get-archive-file-name() {
  cat .archive-file-name
}

get-encrypted-file-name() {
  cat .encrypted-file
}

set -e

if [ -z "${ENVIRONMENT}" ]; then
  echo "==# I need an environment to work with"
  exit 1
fi

if [ -z "${DOMAIN_NAME}" ]; then
  echo "==# I need a domain name to work with"
  exit 1
fi

if [ -z "${DATA_MOUNT_POINT}" ]; then
  DATA_MOUNT_POINT="/data"
fi

if [ -z "${BUCKET}" ]; then
  echo "==# I need an S3 bucket to work with"
  exit 1
fi

if [ -z "${AWS_ACCESS_KEY_ID}" ]  ||
   [ -z "${AWS_DEFAULT_REGION}" ] ||
   [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
  echo "==# I expected AWS credentials to work with, user better have aws-cli configured"
fi

if [ -z "${ENCRYPTION_PASSWORD}" ]; then
  echo "I need a password to encrypt the DB backups"
  exit 1
fi

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."

"${script_dir}/fs/mount-efs.sh" "${ENVIRONMENT}" "${DOMAIN_NAME}" "${DATA_MOUNT_POINT}"

"${script_dir}/fs/archive.sh" "${DATA_MOUNT_POINT}/${ENVIRONMENT}"

"${script_dir}/fs/umount-efs.sh" "${ENVIRONMENT}" "${DATA_MOUNT_POINT}"

"${script_dir}/crypto/encrypt-file.sh" "${ENCRYPTION_PASSWORD}" "$( get-archive-file-name )"

echo "$( get-encrypted-file-name )" > .s3-file-name
"${script_dir}/aws/upload-file-to-s3.sh" "${BUCKET}"

rm -f "$( get-archive-file-name )"
rm -f "$( get-encrypted-file-name )"
