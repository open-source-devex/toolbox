#!/usr/bin/env bash

get-db-instance() {
  aws rds describe-db-instances \
    | jq ".DBInstances[] | select(.DBInstanceIdentifier | contains(\"$1\"))"
}

set -e

ENVIRONMENT=$1

if [ -z "${ENVIRONMENT}" ]; then
  echo "==# I need an environment to work with"
  exit 1
fi

script_dir="$( cd "$( dirname "${0}" )" && pwd )"

db_instance_data="$( get-db-instance "${ENVIRONMENT}" )"
db_instance="$( echo "${db_instance_data}" | jq -r '.DBInstanceIdentifier' )"
db_instance_sec_group="$( echo "${db_instance_data}" | jq -r '.DBSecurityGroups[] .DBSecurityGroupName' )"
snapshop_id="${db_instance}-$( date +%Y%m%d%H%M%S )"

echo "==> Creating snapshot of ${db_instance}"
aws rds create-db-snapshot --db-instance-identifier "${db_instance}" --db-snapshot-identifier "${snapshop_id}"

echo "==> Waiting for snapshop ${snapshop_id} to be ready"
aws rds wait db-snapshot-completed --db-snapshot-identifier "${snapshop_id}"

echo "${db_instance_data}" > .db-instance-data.json
echo "${snapshop_id}" > .snapshot-id
echo "==> Snapshot ${snapshop_id} ready"
