#! /bin/bash

set -e

SSH_KEY_FILE=$1

if [[ -z "${SSH_KEY_FILE}" ]]; then
  echo "==# Need an SSH key to install"
  exit 1
fi

chmod 600 ${SSH_KEY_FILE}
mkdir -p ~/.ssh
ln -s ${SSH_KEY_FILE} ~/.ssh/id_rsa

echo "==> Key installed"
