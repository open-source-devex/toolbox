#!/usr/bin/env bash

SCRIPT_NAME=$( basename "$0" )

usage() {

    echo "==# Usage: ${SCRIPT_NAME} hosted-zone-id record-set-name record-set-identifier record-value record-type record-ttl record-region"
}

set -e

HOSTED_ZONE_ID="$1"
RECORD_SET_NAME="$2"
RECORD_SET_IDENTIFIER="$3"
RECORD_VALUE="$4"
RECORD_TYPE="$5"
RECORD_TTL="$6"
RECORD_REGION="$7"

if [ -z "${HOSTED_ZONE_ID}" ]; then

    echo "==# Need a hosted zone id as first argument"
    usage
    exit 1

fi

if [ -z "${RECORD_SET_NAME}" ]; then

    echo "==# Need a record set name as second argument"
    usage
    exit 1

fi

if [ -z "${RECORD_SET_IDENTIFIER}" ]; then

    echo "==# Need a record set identifier for the record name as third argument"
    usage
    exit 1

fi

if [ -z "${RECORD_VALUE}" ]; then

    echo "==# Need a record value as fourth argument"
    usage
    exit 1

fi

if [ -z "${RECORD_TYPE}" ]; then

    echo "==# Need a record type as fifth argument"
    usage
    exit 1

fi

if [ -z "${RECORD_TTL}" ]; then

    echo "==# Need a record ttl as sixth argument"
    usage
    exit 1

fi

if [ -z "${RECORD_REGION}" ]; then

    echo "==# Need a record region as seventh argument"
    usage
    exit 1

fi

echo "==> Adding record"
CHANGE_INFO_ID="$( aws route53 change-resource-record-sets --hosted-zone-id "${HOSTED_ZONE_ID}" --change-batch '
{
  "Changes": [
    {
      "Action": "CREATE",
      "ResourceRecordSet": {
          "Name": "'"${RECORD_SET_NAME}"'.",
          "Type": "'"${RECORD_TYPE}"'",
          "SetIdentifier": "'"${RECORD_SET_IDENTIFIER}"'",
          "Region": "'"${RECORD_REGION}"'",
          "TTL": '"${RECORD_TTL}"',
          "ResourceRecords": [
            {
              "Value": "'"${RECORD_VALUE}"'"
            }
          ]
        }
    }
  ]
}
' | jq -r '.ChangeInfo .Id')"

echo "==> Waiting for change to complete"
aws route53 wait resource-record-sets-changed --id "${CHANGE_INFO_ID}"


