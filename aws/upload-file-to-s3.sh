#!/usr/bin/env bash

get-s3-file-name() {
  cat ".s3-file-name"
}

set -e

BUCKET=$1
FILE=$2

if [ -z "${BUCKET}" ]; then
  echo "==# Need an S3 bucket to work with"
  exit 1
fi

if [ -z "${FILE}" ]; then
  FILE="$( get-s3-file-name )"
  if [ -z "${FILE}" ]; then
    echo "==# I need an file to work with"
    exit 1
  fi
fi



aws s3 cp "${FILE}" s3://"${BUCKET}/${FILE}"
