#!/usr/bin/env bash

ENVIRONMENT=$1

if [ -z "${ENVIRONMENT}" ]; then
  echo "==# I need an environment to work with"
  exit 1
fi

dump_dir="${ENVIRONMENT}-sql-files-$( date +%Y%m%d%H%M%S )"
rm -rf "${dump_dir}"
mkdir -p "${dump_dir}"

dbs="$(
  mysql -N -e "show databases" \
    | grep -v information_schema \
    | grep -v performance_schema \
    | grep -v innodb \
    | grep -v mysql \
    | grep -v sys \
    | grep -v tmp \
)"

while read db; do
  dumpFile="${dump_dir}/${db}.sql"

  echo "==> Dumping db ${db} to $( basename ${dumpFile} )"

  mysqldump --default-character-set=utf8 \
    --add-drop-table \
    --routines \
    --events \
    "${db}" > "${dumpFile}"
done <<< "${dbs}"

echo "==> Archiving $( basename ${dumpFile} )"
tar cjf "${dump_dir}.tar.bz" ${dump_dir}

echo "==> Archive ${dump_dir}.tar.bz complete"
echo "${dump_dir}.tar.bz" > .db-dump-file-name
