#!/usr/bin/env bash

set -e

script_dir="$( cd "$( dirname "${0}" )" && pwd )"

if [[ -z "${BOT_NAME}" ]]; then
  BOT_NAME="bot"
fi

mkdir -p ~/.ssh
cp ${script_dir}/configuration/ssh_config ~/.ssh/config
sed -i -e "s/_BOT_NAME_/${BOT_NAME}/" ~/.ssh/config
