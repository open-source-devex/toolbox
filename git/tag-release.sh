#!/usr/bin/env bash

RELEASE=${1}

if [[ -z "${RELEASE}" ]]; then
    echo "==# Need RELEASE as 1st arg"
    exit 1
fi

echo "==> Tagging ${RELEASE}"
git tag --annotate ${RELEASE} --message "Release ${RELEASE}"
git push --tags
