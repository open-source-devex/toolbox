#!/usr/bin/env bash

set -e

USER_FILE="$1"

stdin_dump_file="/tmp/user-stdin-dump"

if [[ -z "${USER_FILE}" ]]; then
  echo "==> No input file given, will read from stdin"
  echo "$(</dev/stdin)" > ${stdin_dump_file}
  USER_FILE="${stdin_dump_file}"
fi

if ! cat ${USER_FILE} | jq -e . >/dev/null 2>&1; then
    echo "==# Failed to parse JSON content in ${USER_FILE}"
    exit 1
fi

name=$( jq -r '.name' ${USER_FILE} )
uid=$( jq -r '.uid' ${USER_FILE} )
primary_group=$( jq -r '.primary_group' ${USER_FILE} )
groups=$( jq -r '.groups | join(",")' ${USER_FILE} )
sudoer=$( jq -r '.sudoer' ${USER_FILE} )
ssh_authorized_keys=$( jq -r '.ssh_authorized_keys[]' ${USER_FILE} )

# Create user
adduser -m \
  -g "${primary_group}" \
  -G "${groups}" \
  -u "${uid}" \
  "${name}"

# Setup SSH access
ssh_dir="/home/${name}/.ssh"
mkdir -p "${ssh_dir}"
echo "${ssh_authorized_keys}" > "${ssh_dir}/authorized_keys"
chmod 0600 "${ssh_dir}/authorized_keys"
chown -R "${name}:${primary_group}" "/home/${name}"

# Setup sudoer
if [[ "${sudoer}" == "true" ]]; then
  echo "${name} ALL = NOPASSWD: ALL" > "/etc/sudoers.d/${uid}-${name}"
fi

echo "==> User ${name} created"
