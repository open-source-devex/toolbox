#!/usr/bin/env bash

set -e

script_dir="$( cd "$( dirname "${0}" )" && pwd )"

if [[ -z "${BOT_NAME}" ]]; then
  BOT_NAME="bot"
fi

"${script_dir}/setup-ssh.sh"

"${script_dir}/setup-git.sh"

"${script_dir}/setup-aws.sh"

"${script_dir}/setup-terraform-remote.sh"

echo "==> ${BOT_NAME} is all set to work"
