#!/usr/bin/env bash

PASS=$1
FILE=$2

if [ -z "${PASS}" ]; then
  echo "==# I need a password to encrypt the file with"
  exit 1
fi

if [ -z "${FILE}" ]; then
  echo "==# I need a file to encrypt"
  exit 1
fi

output_file="${FILE}.enc"
echo "==> Encrypting ${FILE}"
openssl aes-256-cbc -e -k "${PASS}" -in "${FILE}" -out "${output_file}"
echo "${output_file}" > .encrypted-file
echo "==> File ${FILE} encrypted to ${output_file}"
