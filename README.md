# Toolbox

Collection of tools to automate operational tasks.

## Users

### create-group.sh

This script expects a json file with the group configuration.
That file should look like this:
```json
{
  "name": "developers",
  "gid": "2000"
}
```

### create-user.sh

This script expects a json file with the user configuration.
That file should look like this:
```json
{
  "name": "miguel",
  "uid": "3000"
  "primary_group": "developers",
  "groups": ["users"],
  "sudoer": "true",
  "ssh_authorized_keys": ["ssh-rsa AAAAB3N...+rOriYw== miguelferreira@me.com"]
}
```

Note that in this example we are adding the user `miguel` to group `develoeprs`, and that requires that the group `developers` be created first.
