#!/usr/bin/env bash

get-instance-data() {
  cat .db-instance-data.json
}

get-snapshot-id() {
  cat .snapshot-id
}

get-db-endpoint() {
  aws rds describe-db-instances | jq -r ".DBInstances[] | select(.DBInstanceIdentifier == \"$1\") .Endpoint .Address"
}


set -e

SNAPSHOT_ID=$1

if [ -z "${SNAPSHOT_ID}" ]; then
  SNAPSHOT_ID="$( get-snapshot-id )"
  if [ -z "${SNAPSHOT_ID}" ]; then
    echo "==# I need a snapshot id to work with"
    exit 1
  fi
fi

db_instance="restored-${SNAPSHOT_ID}"
db_instance_data="$( get-instance-data )"
db_instance_subnet_group="$( echo "${db_instance_data}" | jq -r '.DBSubnetGroup .DBSubnetGroupName' )"

echo "==> Restoring DB snapshot ${SNAPSHOT_ID}"
aws rds restore-db-instance-from-db-snapshot \
  --db-snapshot-identifier "${SNAPSHOT_ID}" \
  --db-instance-identifier "${db_instance}" \
  --db-subnet-group-name "${db_instance_subnet_group}" \
  --db-instance-class db.t2.micro \
  --storage-type standard \
  --no-multi-az > /dev/null

echo "==> Waiting for DB instance to be ready"
aws rds wait db-instance-available --db-instance-identifier "${db_instance}"
echo "${db_instance}" > .restored-db-instance-id

db_endpoint="$( get-db-endpoint "${db_instance}" )"
echo "${db_endpoint}" > .db-endpoint
echo "==> DB instance ${db_instance} is ready and available at:"
echo "${db_endpoint}"
