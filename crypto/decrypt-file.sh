#!/usr/bin/env bash

PASS=$1
FILE=$2

if [ -z "${PASS}" ]; then
  echo "==# I need a password to decrypt the file with"
  exit 1
fi

if [ -z "${FILE}" ]; then
  echo "==# I need a file to decrypt"
  exit 1
fi

output_file="$( echo "${FILE}" | sed -e "s/.enc$//" )"

if [ "${output_file}" == "${FILE}" ]; then
  output_file="${FILE}-$( date +%Y%m%d%H%M%S ).dec"
fi

echo "==> Decrypting "${FILE}" "
openssl aes-256-cbc -d -k "${PASS}" -in "${FILE}" -out "${output_file}"
echo "${output_file}" > .decrypted-file
echo "==> File ${FILE} decrypted to ${output_file}"
